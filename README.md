
# Dr. Web test task

## How to run the project

1. `cd dr_web_test_task`
2. `npm install`
3. `npm run build`
4. `npm run server`

Valid credentials are:

1. Login: `alanSmithee`
2. Password: `alan1234`

Note: While testing in in IE, please close any other browser with this page,
and also, change the user agent string in devtools to the target browser
to prevent BrowserSync set response headers twice
