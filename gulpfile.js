var browserSync = require("browser-sync"),
    gulp = require("gulp"),
    gulpClean = require("gulp-clean-css"),
    gulpConcat = require("gulp-concat"),
    gulpDest = require("gulp-dest"),
    gulpIf = require("gulp-if"),
    gulpRmRf = require("gulp-rimraf"),
    gulpLess = require("gulp-less"),
    gulpSourceMaps = require("gulp-sourcemaps"),
    gulpUglify = require("gulp-uglify");

var normalize = "./node_modules/normalize.css/normalize.css";
var jquery = "node_modules/jquery/dist/jquery.min.js";
var underscore = "node_modules/underscore/underscore-min.js";

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

gulp.task("html", function() {

  return gulp.src("./src/*.html")
  .pipe(gulp.dest("build/"))
  .pipe(browserSync.stream());

});


gulp.task("css:build", function() {

  return gulp.src([normalize, "./src/css/index.less"])

    .pipe(gulpSourceMaps.init())

    .pipe(gulpLess())
    .pipe(postcss([ autoprefixer() ]))
    .pipe(gulpConcat("main.min.css"))

    .pipe(gulpSourceMaps.write())

    .pipe(gulp.dest("build/css/"));

});

gulp.task("js:build", function() {

  var modernizr = "./src/js/modernizr.js";

  return gulp.src([modernizr, jquery, underscore, "./src/js/index.js"])

    .pipe(gulpSourceMaps.init())

    .pipe(gulpConcat("main.min.js"))
    //.pipe(gulpUglify())

    .pipe(gulpSourceMaps.write())

    .pipe(gulp.dest("build/js/"));

});

gulp.task("fonts", function() {

 return gulp.src("./src/fonts/**/*.{eot,woff,woff2,svg,ttf,otf}")
    .pipe(gulp.dest("build/fonts/"));

});

gulp.task("images", function() {
  return gulp.src("./src/images/**/*.{jpg,png,svg}")
    .pipe(gulp.dest("build/images"));
})

gulp.task("watch", function() {

  gulp.watch("./src/*.html", ["html"]);

  gulp.watch("./src/**/*.less", ["css:build"]);
  gulp.watch("./src/js/**/*.js", ["js:build"]);

});


gulp.task("clean:build", function () {

  gulpRmRf("./build");

});

var mocks = {
  "/validate": function(req, res) {

    res.setHeader("Content-Type", "application/json");

    req.on("data", (chunk) => {
      res.end(JSON.stringify({}));
    });

  }
};

gulp.task("run:server", function () {

  browserSync.init({

    server: {

      baseDir: "build",
      middleware: function(req, res, next) {

        if (mocks[req.url]) {
          mocks[req.url](req, res);
        }
        next();
      },
      routes: {
        "/greeting": "build/greeting.html"
      }

    },
    port: 8080,

  });

});


gulp.task("build",

  [
    "clean:build",
    "fonts",
    "images",
    "js:build",
    "css:build",
    "html"
  ]

);


gulp.task("default",

  [
    "build",
    "watch"
  ]

);
