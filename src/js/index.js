
(function(w, d, $, _) {

  var loginForm = $(".login-form");
  var loginFormFields = loginForm.find(".login-form__field-input");


  var userData = {

    login: "alanSmithee",
    password: "alan1234"

  };

  var validationModule = function() {

    var errorMessages = [
      {
        type: "empty",
        message: "This field cannot be empty"
      },
      {
        type: "wrong",
        message: "Username-password combination doesn't match, please try again"
      }
    ];

    return {

      setErrorMessage: function(message, field) {

        var errorEl = {
          "class": "login-form__error-message"
        };

        $(field).closest("label").addClass("login-form__field--invalid-field");

        if (message.type === "empty") {

          $(field).closest("label").append($("<div/>", errorEl));

          $(field).closest("label")
            .find("." + errorEl["class"] + ":eq(1)").remove();

          $("." + errorEl["class"]).text(message.message);

        }

        if (message.type === "wrong") {

          var commonErrorEl = $.extend(errorEl, {
            id: "common-error" });

          var messageWrong = _.find(errorMessages,
            function(mes) { return mes.type === "wrong" });

          $(field).parents("form")
            .find("label")
            .addClass("login-form__field--invalid-field");

          $(field).parents("form").append($("<div/>", commonErrorEl));

          $(field).parents("form").find("#common-error")
            .text(messageWrong.message);

        }

      },

      removeErrorMessage: function(message, field) {

        if (message.type === "empty") {
          $(field).parents("label").find(".login-form__error-message").remove();
        }

        if (message.type === "wrong") {
          $(field).parents("form").find(" > .login-form__error-message").remove();
        }

        $(field).closest("label").removeClass("login-form__field--invalid-field");

        $(field).closest("label")
          .siblings("label")
          .removeClass("login-form__field--invalid-field");

        return;

      },

      checkEmptyField: function(field) {
        return !$(field).val().length;
      },

      checkCredentials: function(field) {

        var name = $(field).attr("name");
        var className = $(field).attr("class");

        var fieldValue = $("." + className + "[name=\"" + name + "\"]").val();

        if (!fieldValue.length) {
          return;
        }

        if (fieldValue === userData[name]) {
          return true;
        }

        return false;

      },

      validateCreds: function(field) {

        var self = this;

        var errorMessage = _.find(
          errorMessages, function(mes) { return mes.type === "wrong"; }
        );

        if (!self.checkCredentials(field)) {

          self.setErrorMessage(errorMessage, field);

          return false;

        }

        self.removeErrorMessage(errorMessage, field);

        return true;

      },

      validateField: function(field) {

        var self = this;

        var errorMessage = _.find(
          errorMessages, function(mes) { return mes.type === "empty"; }
        );

        if (self.checkEmptyField(field)) {

          self.setErrorMessage(errorMessage, field);

          return false;

        }

        self.removeErrorMessage(errorMessage, field);

        return true;

      }

    };


  };

  var validateForm = function(fields) {

    var validation = validationModule();

    var checkedFields =  _.map(fields, validation.validateField.bind(validation));

    var everyFieldValid = _.every(checkedFields, function(f) {
      return f === true;
    });

    if (!everyFieldValid) {
      return false;
    }

    var checkedCreds = _.map(fields, validation.validateCreds.bind(validation));

    var everyCredValid = _.every(checkedCreds, function(f) {
      return f === true;
    });

    if (!everyCredValid) {

      validation.setErrorMessage({ type: "wrong" }, fields[0]);
      return false;

    }

    return true;

  };


  loginForm.on("submit", function(event) {

    event.preventDefault();

    $(this).addClass("login-form--submitted");

    if (!validateForm(loginFormFields)) {
      return;
    }

    var data = $.extend({}, { login: $(loginFormFields[0]).val(),
        password: $(loginFormFields[1]).val() });

    $.ajax({

      type: "post",
      url: "http://localhost:8080/validate",

      dataType: "json",
      data: JSON.parse(JSON.stringify(data))

    })
    .done(function() {

      window.location.assign(window.location.href + "greeting");

    })
    .fail(function(error) {

      console.error(error);

    });

  });

  loginFormFields.bind("input change", function() {

    var validation = validationModule();

    if ($(this).parents("form").hasClass("login-form--submitted")) {

      validation.validateField(this);

    }

    validation.removeErrorMessage({ type: "wrong" }, this);

  });

})(window, document, jQuery, _);
